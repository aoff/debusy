from django.db import models

# Create your models here.

class PickupDate(models.Model):
    date = models.DateField('date of pickup', unique=True)

    def __str__(self):
        return str(self.date)

class Timeslot(models.Model):
    start = models.TimeField('start of timeslot')
    # open-ended for now

    def __str__(self):
        return self.start.isoformat(timespec='minutes')

    class Meta:
        ordering = ['start']

class Member(models.Model):
    member_number = models.PositiveIntegerField(
        verbose_name='aoff member number',
        blank=True,
        null=True,
    )

class Pickup(models.Model):
    date = models.ForeignKey(PickupDate, on_delete=models.CASCADE)
    time = models.ForeignKey(Timeslot, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
