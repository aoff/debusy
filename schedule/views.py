from django.shortcuts import get_object_or_404, render
from .models import *
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse

def schedule(request, date):
    date = get_object_or_404(PickupDate, date=date)
    member_number = request.POST and request.POST.get('member_number') \
            or None
    member_id = request.session.get('member_id')
    if member_id is not None:
        member = get_object_or_404(Member, pk=member_id)
    elif member_number:
        member = Member()
        request.session['member_id'] = member.id
        request.session.set_expiry(0)
    else:
        member = None
    if member_number and member.member_number != member_number:
        member.member_number = member_number
        member.save()
    timeslots = Timeslot.objects.all()
    for timeslot in timeslots:
        timeslot.sum = len(Pickup.objects.filter(time=timeslot))
        if member:
            try:
                timeslot.own_pickup = \
                        Pickup.objects.filter(
                                date=date,
                                time=timeslot,
                                member=member,
                        ).exists()
            except Pickup.DoesNotExist:
                timeslot.own_pickup = False
        else:
            timeslot.own_pickup = False

    return render(request, 'schedule/schedule.html',
            { 'date': date,
                'timeslots': timeslots,
                })

def submit_pickup(request, date):
    date = get_object_or_404(PickupDate, date=date)
    request.session.set_expiry(0)
    member_id = request.session.get('member_id')
    if not member_id:
        member = Member(member_number=request.POST.get('member_number'))
        member.save()
        request.session['member_id'] = member.id
    else:
        member = get_object_or_404(Member, pk=member_id)
    timeslot = get_object_or_404(Timeslot, pk=request.POST.get('timeslot'))
    Pickup(date=date, time=timeslot, member=member).save()
    return HttpResponseRedirect(reverse('schedule:schedule', args=(date.date,)))

def submit_pickup_removal(request, date):
    date = get_object_or_404(PickupDate, date=date)
    member = get_object_or_404(Member, pk=request.session.get('member_id'))
    timeslot = get_object_or_404(Timeslot, pk=request.POST.get('timeslot'))
    Pickup.objects.filter(date=date, time=timeslot, member=member).delete()
    return HttpResponseRedirect(reverse('schedule:schedule', args=(date.date,)))
