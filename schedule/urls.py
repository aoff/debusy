from django.urls import path, register_converter

from . import views, converters

register_converter(converters.DateConverter, 'date')

app_name = 'schedule'
urlpatterns = [
    path('<date:date>/', views.schedule, name='schedule'),
    path('<date:date>/submit', views.submit_pickup, name='submit_pickup'),
    path('<date:date>/remove', views.submit_pickup_removal, name='submit_pickup_removal'),
]
